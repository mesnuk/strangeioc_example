﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.IO;

public class StartScreenMediator : BaseMediator
{

	[Inject]
	public StartScreenView view { get; set; }

	public override void PreRegister()
	{
		
	}
	public override void OnRegister()
	{
		
		view.LoadView();
	}

	public override void OnRemove()
	{
		
		view.RemoveView();
	}

	public override void OnAppBackButton()
	{
		//Application.Quit();
	}


}
