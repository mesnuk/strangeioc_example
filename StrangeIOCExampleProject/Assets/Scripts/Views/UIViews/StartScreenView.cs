﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StartScreenView : BaseView 
{

    public Text playText;
    public Button play;


    private void AddEventTrigger(GameObject go, EventTriggerType triggerType, UnityAction action)
    {
        go.AddComponent<EventTrigger>();
        EventTrigger trigger = go.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = triggerType;
        entry.callback.AddListener((data) =>
        {
            action();
        });
        trigger.triggers.Add(entry);
    }

    public void LoadView()
    {
        //play.onClick.RemoveAllListeners();
        //play.onClick.AddListener(() =>
        //{
            
        //});

        //AddEventTrigger(back.gameObject, EventTriggerType.PointerDown, () =>
        //{
        //    back.transform.GetChild(0).GetComponent<Text>().color = Color.black;
        //});
        //AddEventTrigger(back.gameObject, EventTriggerType.PointerUp, () =>
        //{
        //    back.transform.GetChild(0).GetComponent<Text>().color = Color.cyan;
        //});
        //AddEventTrigger(back.gameObject, EventTriggerType.PointerExit, () =>
        //{
        //    back.transform.GetChild(0).GetComponent<Text>().color = Color.cyan;
        //});

    }

    public void RemoveView()
    {
       
    }
}
