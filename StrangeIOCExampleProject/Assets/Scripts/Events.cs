using UnityEngine;
using System.Collections;


// ���������� ������ ����
public enum EventGlobal
{
	E_None = 0,
	E_AppUpdate,
	E_AppFixedUpdate,
	E_AppLateUpdate,
	E_AppBackButton,

    //UI
    E_ShowScreen,
    E_HideScreen,
    E_ShowBlocker,
    E_HideBlocker,
    E_ScreenManagerBack,
    E_HideCurrentScreen,
    E_HideAllScreens,
    E_ResetTopPanel,
    E_GetHomeworksStatsCommand,
    E_MoveLibraryPanel,
    E_MoveLibraryPanelInversion,
    E_LibraryCategoriesButtonClick,
    E_LibraryCategoryToTopPanel,
    E_HideLibraryPanelToTopPanel,
    E_HideMenuPanelInTopPanel,
    E_UpdateBooksLikeStatus,
    E_ChangeBookDetailsLanguage,
    E_SelectBookDetails,
    E_ChangeBookDetailsCover,
    E_ProcessBooksSearch,
    E_UpdateBooksSearchResults,
    E_ResetChildClasses,
    E_UpdateChildClasses,
    E_UpdateDownloadedBooks,
    E_LocalizationSelected,


    E_end
}
