﻿using UnityEngine;
using System.Collections;
using System;

public class CoroutineWorker : MonoBehaviour
{
	~CoroutineWorker()
	{
	}

	//public void Update()
	//{
	//	if(isStop)
	//	{
	//		StopAllCoroutines();
	//	}
	//}

	//bool isStop = false;
	//public void StopCoroutines()
	//{
	//	isStop = true;
	//	StopAllCoroutines();
	//}
}

public class CoroutineExecutor : IExecutor
{
	public static CoroutineExecutor instance;

	private CoroutineWorker _worker;
	~CoroutineExecutor()
	{
		instance = null;
		_worker = null;
	}
	public CoroutineExecutor()
	{
		var go = new GameObject("CoroutineWorker");
		_worker = go.AddComponent<CoroutineWorker>();
		instance = this;
		GameObject.DontDestroyOnLoad(go);
	}

	public void Execute(IEnumerator coroutine)
	{
		_worker.StartCoroutine(coroutine);
	}

	public void StopExecution(IEnumerator coroutine)
	{
		_worker.StopCoroutine(coroutine);
	}

    public void StopAllExecution()
    {
        _worker.StopAllCoroutines();
    }
}
