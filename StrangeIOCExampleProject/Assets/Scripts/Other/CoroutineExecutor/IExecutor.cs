﻿using System.Collections;

public interface IExecutor
{

	void Execute(IEnumerator coroutine);

	void StopExecution(IEnumerator coroutine);

    void StopAllExecution();
}
