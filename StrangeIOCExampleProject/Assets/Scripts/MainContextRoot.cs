using UnityEngine;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using PFS.Assets.Scripts.Commands.UI;


public class MainContextRoot : MVCSContext
{
    public MainContextRoot(MonoBehaviour contextView) : base(contextView, ContextStartupFlags.MANUAL_MAPPING)
    {
    }

    // CoreComponents
    protected override void AddCoreComponents()
    {
        base.AddCoreComponents();
        injectionBinder.Unbind<ICommandBinder>(); //Unbind to avoid a conflict!
        injectionBinder.Bind<ICommandBinder>().To<EventCommandBinder>().ToSingleton();
        injectionBinder.Bind<ScreenManager>().ToSingleton();

        injectionBinder.Bind<IExecutor>().To<CoroutineExecutor>().ToSingleton();
     
    }

    // Commands and Bindings
    protected override void MapBindings()
    {
        base.MapBindings();

        // Mediator
        mediationBinder.BindView<StartScreenView>().ToMediator<StartScreenMediator>();
		

        // System Commands
        commandBinder.Bind(ContextEvent.START)
        .To<StartGameCommand>()
        .Pooled()
		.InSequence()
		.Once();

        //UI
        commandBinder.Bind(EventGlobal.E_ShowScreen).To<UISwitchScreensAnimationCommand>().To<UIScreenShowCommand>().Pooled();
        commandBinder.Bind(EventGlobal.E_HideScreen).To<UISwitchScreensAnimationCommand>().To<UIScreenHideCommand>().Pooled();
        commandBinder.Bind(EventGlobal.E_ShowBlocker).To<UIBlockerShowCommand>().Pooled();
        commandBinder.Bind(EventGlobal.E_HideBlocker).To<UIBlockerHideCommand>().Pooled();
        commandBinder.Bind(EventGlobal.E_ScreenManagerBack).To<ScreenManagerBackCommand>().Pooled();
        commandBinder.Bind(EventGlobal.E_HideCurrentScreen).To<ScreenManagerDeleteCurrentScreen>().Pooled();
        commandBinder.Bind(EventGlobal.E_HideAllScreens).To<ScreenManagerDeleteAllScreen>().Pooled();


        // commandBinder.Bind(EventGlobal.E_LoadWWWGameStart).To<UIStartScrenShowCommand>().To<WWWConnectGame>().To<LoadLevelCommand>().To<UIStartScrenHideCommand>().InSequence().Pooled();    
    }
}
