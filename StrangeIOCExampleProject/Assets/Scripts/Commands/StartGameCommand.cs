﻿public class StartGameCommand : BaseCommand
{
    public override void Execute()
    {
        Dispatcher.Dispatch(EventGlobal.E_ShowScreen, UIScreens.UIStartScreen);
    }
}
