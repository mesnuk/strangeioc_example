﻿    public class UIBlockerHideCommand : BaseCommand
    {
        public override void Execute()
        {
            Dispatcher.Dispatch(EventGlobal.E_HideScreen, UIScreens.UIBlockerScreen);
        }
    }
